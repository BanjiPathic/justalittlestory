﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.GameStates
{
	abstract class State
	{
		#region Variables

		protected ContentManager content;
		protected Game1 game;
		protected GraphicsDevice graphics;
		protected State lastState;

		#endregion

		#region Constructor

		public State(Game1 _game, GraphicsDevice _graphics, ContentManager _content, State _lastState)
		{
			game = _game;
			graphics = _graphics;
			content = _content;
			lastState = _lastState;
		}

		#endregion

		#region Public

		public abstract void Draw(GameTime _gameTime, SpriteBatch _spritebatch);
		public abstract void PostUpdate(GameTime _gameTime);
		public abstract void Update(GameTime _gameTime);

		#endregion

	}

}
