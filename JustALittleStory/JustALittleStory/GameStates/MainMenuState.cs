﻿using JustALittleStory.Ingame;
using JustALittleStory.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.GameStates
{
	class MainMenuState : State
	{

		#region Variables

		private List<Component> buttons;



		#endregion

		#region Constructor

		public MainMenuState(Game1 _game, State _lastState, GraphicsDevice _graphics, ContentManager _content) : base(_game, _graphics, _content, _lastState)
		{
			var buttonTexture = _content.Load<Texture2D>("UI/BlankButton");
			int posX = 500;
			buttons = new List<Component>();

			var newGameButton = new Button(buttonTexture, new Vector2(posX, 300), "New Game");
			newGameButton.Click += NewGameButton_Click;
			buttons.Add(newGameButton);

			var fightButton = new Button(buttonTexture, new Vector2(posX, 350), "Fight");
			fightButton.Click += FightButton_Click;
			buttons.Add(fightButton);

			var EditorButton = new Button(buttonTexture, new Vector2(posX, 400), "Editor");
			EditorButton.Click += EditorButton_Click;
			buttons.Add(EditorButton);

			var optionButton = new Button(buttonTexture, new Vector2(posX, 450), "Option");
			optionButton.Click += OptionButton_Click;
			buttons.Add(optionButton);

			var quitButton = new Button(buttonTexture, new Vector2(posX, 500), "Quit");
			quitButton.Click += QuitButton_Click;
			buttons.Add(quitButton);
		}

		private void FightButton_Click(object sender, EventArgs e)
		{
			game.ChangeState(new CombatState(game, this, graphics, content));
		}



		#endregion

		#region Helpers

		private void LoadGameButton_Click(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}


		private void NewGameButton_Click(object sender, EventArgs e)
		{
			game.ChangeState(new GameState(game, graphics, content, this));
		}

		private void OptionButton_Click(object sender, EventArgs e)
		{
			throw new NotImplementedException();
			//game.ChangeState(new OptionState(game, graphics, content, this));
		}


		private void QuitButton_Click(object sender, EventArgs e)
		{
			game.Exit();
		}

		private void EditorButton_Click(object sender, EventArgs e)
		{
			game.ChangeState(new EditorState(game, graphics, content, this));
		}

		#endregion


		#region Public

		public override void Draw(GameTime _gameTime, SpriteBatch _spriteBatch)
		{

			_spriteBatch.Begin();

			_spriteBatch.DrawString(CM.I.spriteFont, "MainMenu", new Vector2(10, 10), Color.Black);

			foreach (var butt in buttons)
			{
				butt.Draw(_gameTime, _spriteBatch);
			}

			_spriteBatch.End();

		}

		public override void PostUpdate(GameTime _gameTime)
		{
			throw new NotImplementedException();
		}

		public override void Update(GameTime _gameTime)
		{

			foreach (var butt in buttons)
			{
				butt.Update(_gameTime);
			}
		}

		#endregion


	}

}
