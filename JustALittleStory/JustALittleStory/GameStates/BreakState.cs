﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustALittleStory.Ingame;
using JustALittleStory.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace JustALittleStory.GameStates
{
	class BreakState : State
	{

		#region Variables

		List<Component> buttons;

		#endregion

		#region Constructor		

		public BreakState(Game1 _game, GraphicsDevice _graphics, ContentManager _content, State _lastState) : base(_game, _graphics, _content, _lastState)
		{

			var buttonTexture = _content.Load<Texture2D>("UI/BlankButton");
			int posX = 500;
			buttons = new List<Component>();

			var BackButton = new Button(buttonTexture, new Vector2(posX, 300), "Back");
			BackButton.Click += BackButton_Click;
			buttons.Add(BackButton);

			var optionButton = new Button(buttonTexture, new Vector2(posX, 400), "Option");
			optionButton.Click += OptionButton_Click;
			buttons.Add(optionButton);

			var quitButton = new Button(buttonTexture, new Vector2(posX, 450), "Quit");
			quitButton.Click += QuitButton_Click;
			buttons.Add(quitButton);

		}

		private void QuitButton_Click(object sender, EventArgs e)
		{
			game.Exit();
		}

		private void OptionButton_Click(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		private void BackButton_Click(object sender, EventArgs e)
		{
			game.ChangeState(lastState);
		}

		#endregion

		#region Helpers
		#endregion


		#region Public		

		public override void Draw(GameTime _gameTime, SpriteBatch _spritebatch)
		{
			_spritebatch.Begin();

			_spritebatch.DrawString(CM.I.spriteFont, "MainMenu", new Vector2(10, 10), Color.Black);

			foreach (var butt in buttons)
			{
				butt.Draw(_gameTime, _spritebatch);
			}

			_spritebatch.End();
		}

		public override void PostUpdate(GameTime _gameTime)
		{
			throw new NotImplementedException();
		}

		public override void Update(GameTime _gameTime)
		{

			foreach (var butt in buttons)
			{
				butt.Update(_gameTime);
			}
		}
		
		#endregion


		#region Events
		#endregion





	}
}
