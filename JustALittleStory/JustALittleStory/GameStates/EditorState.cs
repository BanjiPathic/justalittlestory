﻿using JustALittleStory.Ingame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.GameStates
{
	class EditorState : State
	{


		#region Variables

		#endregion

		#region Constructor		

		public EditorState(Game1 _game, GraphicsDevice _graphics, ContentManager _content, State _lastState) : base(_game, _graphics, _content, _lastState)
		{


			GS.I.editor = new Editor.Editor();

			GS.I.camera1 = new Camera3rdPerson(_graphics);
			GS.I.camera2 = new CameraFightMode(_graphics);
			GS.I.trees = new List<Tree>();
			//GS.I.rockes = new List<Rock>();
		}

		#endregion

		#region Helpers
		#endregion


		#region Public		

		public override void Draw(GameTime _gameTime, SpriteBatch _spritebatch)
		{
			
			if (GS.I.editor.GetFightMode())
			{
				GS.I.ground.DrawModel(GS.I.camera2);
				
				foreach (Tree t in GS.I.trees)
				{
					t.DrawModel(GS.I.camera2);
				}
				/*
				foreach (Rock r in GS.I.rockes)
				{
					r.DrawModel(GS.I.camera2);
				}
				*/
				GS.I.editor.DrawModel(GS.I.camera2);
			}
			else
			{
				GS.I.ground.DrawModel(GS.I.camera1);
				
				foreach (Tree t in GS.I.trees)
				{
					t.DrawModel(GS.I.camera1);
				}
				/*
				foreach (Rock r in GS.I.rockes)
				{
					r.DrawModel(GS.I.camera1);
				}
				*/
				GS.I.editor.DrawModel(GS.I.camera1);
			}
		
			_spritebatch.Begin();

			if (GS.I.editor != null)
			{
				string name = GS.I.editor.Name;
				if (GS.I.editor.GetFightMode())
				{
					name += " fight cam " ;

				}
				else
				{
					name += " free cam" ;
				}
				_spritebatch.DrawString(CM.I.spriteFont, name, new Vector2(10, 10), Color.Black);
			}
			
		

			_spritebatch.End();
		}

		public override void PostUpdate(GameTime _gameTime)
		{
			throw new NotImplementedException();
		}

		public override void Update(GameTime _gameTime)
		{
			GS.I.editor.Update(_gameTime);
			GS.I.camera1.Update(GS.I.editor.world);
			GS.I.camera2.Update(GS.I.editor.world);
		}

		#endregion

	}

}
