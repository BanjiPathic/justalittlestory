﻿using JustALittleStory.Ingame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.GameStates
{
	class CombatState : State
	{

		#region Variables
		#endregion

		#region Constructor		

		public CombatState(Game1 _game, State _lastState, GraphicsDevice _graphics, ContentManager _content) : base(_game, _graphics, _content, _lastState)
		{
			GS.I.editor = new Editor.Editor();

			GS.I.fightPlayer = new PlayerFightMode(CM.I.player);
			GS.I.camera1 = new Camera3rdPerson(_graphics);
			GS.I.camera2 = new CameraFightMode(_graphics);

			GS.I.enemies = new List<Enemy>();

			GS.I.enemies.Add(new Enemy(CM.I.player, GS.I.fightPlayer.playerWorldPosition 
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Left*10)
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Forward * 12)
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Up * 4)));

			GS.I.enemies.Add(new Enemy(CM.I.player, GS.I.fightPlayer.playerWorldPosition
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Right * 10)
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Forward * 12)
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Up * 4)));

			GS.I.enemies.Add(new Enemy(CM.I.player, GS.I.fightPlayer.playerWorldPosition
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Forward * 15)
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Up * 4)));

			GS.I.enemies.Add(new Enemy(CM.I.player, GS.I.fightPlayer.playerWorldPosition
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Forward * 15)
				* Matrix.CreateTranslation(GS.I.fightPlayer.playerWorldPosition.Up * 4)));

			GS.I.editor.LoadScene("Game2.txt");





		}

		#endregion

		#region Helpers
		#endregion


		#region Public

		public override void Draw(GameTime _gameTime, SpriteBatch _spritebatch)
		{
			GS.I.fightPlayer.DrawModel(GS.I.camera2);

			GS.I.ground.DrawModel(GS.I.camera2);

			foreach (Tree t in GS.I.trees)
			{
				t.DrawModel(GS.I.camera2);
			}

			foreach (Enemy e in GS.I.enemies)
			{
				e.DrawModel(GS.I.camera2);
			}
			/*
			foreach (Rock r in GS.I.rockes)
			{
				r.DrawModel(GS.I.camera2);
			}
			*/
			_spritebatch.Begin();

			_spritebatch.DrawString(CM.I.spriteFont, "fightMode" , new Vector2(10, 10), Color.Black);
		
			_spritebatch.End();

		}

		public override void PostUpdate(GameTime _gameTime)
		{
			throw new NotImplementedException();
		}

		public override void Update(GameTime _gameTime)
		{
			GS.I.fightPlayer.Update(_gameTime);
			GS.I.camera2.Update(GS.I.fightPlayer.playerWorldPosition);
		}

		#endregion


		#region Events
		#endregion

	}

}
