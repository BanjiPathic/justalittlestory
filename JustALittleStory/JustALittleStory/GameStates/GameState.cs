﻿using JustALittleStory.Ingame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.GameStates
{
	class GameState : State
	{

		#region Variables

		Camera3rdPerson cam;
		Player player;

		Companion wolf;

		#endregion

		#region Constructor		

		public GameState(Game1 _game,  GraphicsDevice _graphics, ContentManager _content, State _lastState) : base(_game, _graphics, _content, _lastState)
		{
			GS.I.player = new Player(CM.I.player);

			Matrix wolfPos = GS.I.player.playerWorldPosition * Matrix.CreateTranslation(GS.I.player.playerWorldPosition.Right * 3);
			GS.I.wolf = new Companion(CM.I.wolf, wolfPos);
			GS.I.player.setWolf(GS.I.wolf);


			GS.I.editor = new Editor.Editor();

		
			GS.I.camera1 = new Camera3rdPerson(_graphics);
			GS.I.camera2 = new CameraFightMode(_graphics);
			GS.I.editor.LoadScene("Game1.txt");


			GS.I.player = new Player(CM.I.player);
		}

		#endregion

		#region Helpers
		#endregion


		#region Public

		public override void Draw(GameTime _gameTime, SpriteBatch _spriteBatch)
		{
			


			
			GS.I.ground.DrawModel(GS.I.camera1);



			foreach (Tree t in GS.I.trees)
			{
				t.DrawModel(GS.I.camera1);
			}

			GS.I.player.DrawModel(GS.I.camera1);

			GS.I.wolf.DrawModel(GS.I.camera1);

			_spriteBatch.Begin();

			_spriteBatch.DrawString(CM.I.spriteFont, "Game", new Vector2(10, 10), Color.Black);

			_spriteBatch.End();
		}

		public override void PostUpdate(GameTime _gameTime)
		{

		}

		public override void Update(GameTime _gameTime)
		{
			KeyboardState kState = Keyboard.GetState();
			if (kState.IsKeyDown(Keys.Escape))
			{
				game.ChangeState(new BreakState(game, graphics, content, this));
			}

			GS.I.player.Update(_gameTime);
			GS.I.camera1.Update(GS.I.player.playerWorldPosition);
		}

		#endregion


		#region Events
		#endregion


	}

}
