﻿using JustALittleStory.Ingame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace JustALittleStory.Editor
{
	class Editor
	{
		#region Variables


		#region KeyWords

		#endregion

		char endSymbol = '!';
		public string Name = "Editor";

		float rotationRate = 0.5f;
		float speed = 30;
		public Matrix world;
		KeyboardState lastKState;

		 int currentList = 0;
		 int currentModel = 0;

		Model[] models;
		bool fightMode = false;
		

		#endregion

		#region Constructor

		public Editor()
		{

			models = CM.I.bigRocks;

			world = Matrix.Identity;

			lastKState = Keyboard.GetState();

			Name = "Editor";

		}

		#endregion

		#region Processes

		static  string ProcessRead(string fileName)
		{
			 return Task.Run(() => ReadTextAsync(fileName)).Result;
		}

		static async Task<string> WriteTextAsync(string fileName, string text)
		{
			byte[] encodedText = Encoding.Unicode.GetBytes(text);

			System.Diagnostics.Debug.WriteLine("Writing to file: " + fileName);

			StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
			
			string fullName ="";
			StorageFile file = null;

			bool con = true;
			for (int i = 0; con; i++)
			{
				string fullNameTmp = fileName;
				fullNameTmp += i + ".txt";
				StorageFile fileTmp = (StorageFile)await storageFolder.TryGetItemAsync(fullNameTmp);
				if (fileTmp == null)
				{
					fileTmp = await storageFolder.CreateFileAsync(fullNameTmp);
					fullName = fullNameTmp;
					file = fileTmp;
					con = false;
				}
			}

			
			System.Diagnostics.Debug.WriteLine("Full path: " + file.Path);
			
			await FileIO.WriteBytesAsync(file, encodedText);
			return "saved " + fullName;
		}

		static async Task<string> ReadTextAsync(string fileName)
		{
			StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
			StorageFile file = (StorageFile)await storageFolder.TryGetItemAsync(fileName);

			if (file == null)
			{
				file = await storageFolder.CreateFileAsync(fileName);
			}

			string filePath = file.Path;

			using (FileStream sourceStream = new FileStream(filePath,FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true))
			{
				StringBuilder sb = new StringBuilder();

				byte[] buffer = new byte[0x1000];
				int numRead;
				while ((numRead = await sourceStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
				{
					string text = Encoding.Unicode.GetString(buffer, 0, numRead);
					sb.Append(text);
				}

				return sb.ToString();
			}
		}

		static string ProcessWrite(string fileName, string text)
		{
			return Task.Run(() => WriteTextAsync(fileName, text)).Result;
		}

		#endregion


		#region Helpers

		private void KeyBoardInput(float dt)
		{
			KeyboardState kstate = Keyboard.GetState();


			//Player Movement
			if (kstate.IsKeyDown(Keys.W)) //W
			{
				world *= Matrix.CreateTranslation(world.Forward * speed * dt);
			}
			if (kstate.IsKeyDown(Keys.S)) //S
			{
				world *= Matrix.CreateTranslation(world.Backward * speed * dt);
			}
			if (kstate.IsKeyDown(Keys.A))
			{
				world *= Matrix.CreateTranslation(world.Left * speed * dt);
			}
			if (kstate.IsKeyDown(Keys.D))
			{
				world *= Matrix.CreateTranslation(world.Right * speed * dt);
			}

			if (kstate.IsKeyDown(Keys.Right)) //D
			{
				Vector3 temp = world.Translation;
				world.Translation = Vector3.Zero;
				world *= Matrix.CreateRotationY(rotationRate * dt);
				world.Translation = temp;

			}
			if (kstate.IsKeyDown(Keys.Left))//A
			{
				Vector3 temp = world.Translation;
				world.Translation = Vector3.Zero;
				world *= Matrix.CreateRotationY(-rotationRate * dt);
				world.Translation = temp;
			}

			if (lastKState.IsKeyDown(Keys.Tab) && kstate.IsKeyUp(Keys.Tab))
			{
				if (currentList == 0)
				{
					models = CM.I.bigRocks;
				}
				else if (currentList == 1)
				{
					models = CM.I.rocks;
				}
				else if (currentList == 2)
				{
					models = CM.I.deadTrees;
				}
				else if (currentList == 3)
				{
					models = CM.I.trees;
				}

				if (currentList < 3)
				{
					currentList++;
				}
				else
				{
					currentList = 0;
				}

				if (currentModel >= models.Length)
				{
					currentModel = 0;
				}
			}

			if (lastKState.IsKeyDown(Keys.Q) && kstate.IsKeyUp(Keys.Q))
			{
				if ((currentModel + 1) < models.Length)
				{
					currentModel++;
				}
				else
				{
					currentModel = 0;
				}
			}


			if (lastKState.IsKeyDown(Keys.E) && kstate.IsKeyUp(Keys.E))
			{
				GS.I.trees.Add(new Tree(models[currentModel], world, currentModel, currentList));

			}

			if ((kstate.IsKeyDown(Keys.LeftControl) || kstate.IsKeyDown(Keys.RightControl)) && lastKState.IsKeyDown(Keys.Y) && kstate.IsKeyUp(Keys.Y))
			{
				SaveScene();
			}

			if ((kstate.IsKeyDown(Keys.LeftControl) || kstate.IsKeyDown(Keys.RightControl)) && lastKState.IsKeyDown(Keys.O) && kstate.IsKeyUp(Keys.O))
			{
				LoadScene("Game1.txt");
			}

			if (kstate.IsKeyDown(Keys.Up))
			{
				GS.I.camera1.Zoom(1);
			}
			if (kstate.IsKeyDown(Keys.Down))
			{
				GS.I.camera1.Zoom(-1);
			}
			if (lastKState.IsKeyDown(Keys.CapsLock) && kstate.IsKeyUp(Keys.CapsLock) )
			{
				fightMode = !fightMode;
			}
			if ((kstate.IsKeyDown(Keys.LeftControl) || kstate.IsKeyDown(Keys.RightControl)) && lastKState.IsKeyDown(Keys.Z) && kstate.IsKeyUp(Keys.Z) && (GS.I.trees.Count>0))
			{
				GS.I.trees.Remove(GS.I.trees[GS.I.trees.Count - 1]);
			}

			lastKState = kstate;
		}

		private string WritePlayer()
		{
			string toFill = "";

			if (GS.I.player != null)
			{
				throw new Exception();
				toFill += "player begin" + endSymbol + "\n";
				toFill += "position#";
				toFill += GS.I.player.playerWorldPosition.ToString();
				toFill += endSymbol;
				toFill += "\n";
				toFill += "player end" + endSymbol + "\n\n";
			}
			else
			{
				toFill += "player none" + endSymbol + "\n\n";
			}
			return toFill;

		}

		private string WriteCamera()
		{
			string toFill = "";


			if (GS.I.camera1 != null)
			{
				toFill += "camera1 begin" + endSymbol + "\n";
				toFill += WriteMatrix(GS.I.camera1.view);
				toFill += WriteMatrix(GS.I.camera1.projection);
				toFill += WriteVector3(GS.I.camera1.camDirection);
				toFill += WriteVector3(GS.I.camera1.camPosition);
				toFill += "camera1 end" + endSymbol + "\n\n";
			}
			if (GS.I.camera2 != null)
			{
				toFill += "camera2 begin" + endSymbol + "\n";
				toFill += WriteMatrix(GS.I.camera2.view);
				toFill += WriteMatrix(GS.I.camera2.projection);
				toFill += WriteVector3(GS.I.camera2.camDirection);
				toFill += WriteVector3(GS.I.camera2.camPosition);
				toFill += "camera2 end" + endSymbol + "\n\n";
			}
			if (GS.I.camera1 == null && GS.I.camera2 == null)
			{
				toFill += "camera none" + endSymbol + "\n\n";
			}
			return toFill;
		}

		private string WriteTrees()
		{
			string toFill ="";

			if (GS.I.trees != null && GS.I.trees.Count > 0)
			{
	
				toFill += "trees begin" + endSymbol + "\n";
			
				foreach(Tree t in GS.I.trees)
				{
					toFill += "tree"+ endSymbol + "\n";
				
					toFill += WriteMatrix(t.position);
					toFill += t.modelID;
					toFill += endSymbol + "\n";
					toFill += t.listID;
					toFill += endSymbol + "\n";

				}
				toFill += "trees end" + endSymbol + "\n\n";
			}
			else
			{
				toFill += "trees none" + endSymbol + "\n\n";
			}

			return toFill;
		}

		private string WriteMatrix(Matrix toWrite)
		{
			string toFill = "";
			toFill += toWrite.M11;
			toFill += "#";
			toFill += toWrite.M12;
			toFill += "#";
			toFill += toWrite.M13;
			toFill += "#";
			toFill += toWrite.M14;
			toFill += "#";
			toFill += toWrite.M21;
			toFill += "#";
			toFill += toWrite.M22;
			toFill += "#";
			toFill += toWrite.M23;
			toFill += "#";
			toFill += toWrite.M24;
			toFill += "#";
			toFill += toWrite.M31;
			toFill += "#";
			toFill += toWrite.M32;
			toFill += "#";
			toFill += toWrite.M33;
			toFill += "#";
			toFill += toWrite.M34;
			toFill += "#";
			toFill += toWrite.M41;
			toFill += "#";
			toFill += toWrite.M42;
			toFill += "#";
			toFill += toWrite.M43;
			toFill += "#";
			toFill += toWrite.M44;
			toFill += endSymbol;
			toFill += "\n";
			return toFill;
		}

		private string WriteVector3(Vector3 _toWrite)
		{
			string toFill = "";
			toFill += _toWrite.X;
			toFill += "#";
			toFill += _toWrite.Y;
			toFill += "#";
			toFill += _toWrite.Z;
			toFill += endSymbol;
			toFill += "\n";
			return toFill;
		}


		private string WriteEditor()
		{
			string toFill = "";

			toFill += "editor begin" + endSymbol + "\n";
			toFill += WriteMatrix(world);
			toFill += "editor end" + endSymbol + "\n\n";

			return toFill;
		}

		private string GetRow(char[] _toProcess, int _index)
		{ // diese Methode soll eine zeile bis zum end symbol aus lesen 
			int len = _toProcess.Length;
			string store = "";
			bool con = true;

			if (_index < len && _index >= 0)
			{
				for (int i = _index; i < len && con; i++)
				{
					if (_toProcess[i] == endSymbol)
					{
						con = !con;
					}
					else
					{
						store += _toProcess[i];
					}
				}
			}

			return store;
		}

		private Matrix ReadMatrix(string _info)
		{

			char[] info = _info.ToCharArray();

			int len = info.Length;

			string[] toDo = new string[16];
			int lauf = 0;

			for (int j = 0; j < _info.Length; j++)
			{
				if (_info[j] == '#')
				{
					lauf++;
				}
				else
				{
					toDo[lauf] += _info[j];
				}
			}

			
			float[] mf = new float[16];
			CultureInfo current = new CultureInfo("en-US");
			float counter = 0f;
			for (int i = 0; i < mf.Length; i++)
			{
				mf[i] = float.Parse(toDo[i], NumberStyles.Float, current);
				counter += Math.Abs(mf[i]);
			}
			if (counter < 0.00001)
			{
				throw new Exception();
			}
			Matrix res = new Matrix(mf[0], mf[1], mf[2], mf[3],
						mf[4], mf[5], mf[6], mf[7],
						mf[8], mf[9], mf[10], mf[11],
						mf[12], mf[13], mf[14], mf[15]);

			
			return res;
	
		}

		private Vector3 ReadVector3(string _info)
		{
			char[] info = _info.ToCharArray();

			int len = info.Length;

			string[] toDo = new string[3];
			int lauf = 0;

			for (int j = 0; j < _info.Length; j++)
			{
				if (_info[j] == '#')
				{
					lauf++;
				}
				else
				{
					toDo[lauf] += _info[j];
				}
			}

			float[] mf = new float[3];
			CultureInfo current = new CultureInfo("en-US");
			float counter = 0f;

			for (int i = 0; i < mf.Length; i++)
			{
				mf[i] = float.Parse(toDo[i], NumberStyles.Float, current);
				counter += Math.Abs(mf[i]);
			}
			if (counter < 0.00001)
			{
				throw new Exception();
			}

			Vector3 res = new Vector3(mf[0], mf[1], mf[2]);
			return res;
		}

		private int ReatInt(string _info)
		{
			return int.Parse(_info);
		}

		private void InitalizeGS(string _text)
		{ 
			char[] toProcess = _text.ToCharArray();
			int lineLen = "\n\n".Length;
			int enterLen = "\n".Length;
			int counter = 0;
			string toDo = GetRow(toProcess, counter);

			;

			// Player read and intialize, erstmal nur dummie
			if (toDo.Equals("player none"))
			{
				GS.I.player = null;
				counter += toDo.Length + 1;
				counter += lineLen;

	
			}
			else if(toDo.Equals("player begin"))
			{
				throw new Exception("no player but expected");
			}
			else
			{
	
			}
		
			//camera
			toDo = GetRow(toProcess, counter);
			if (toDo.Equals("camera none"))
			{
				GS.I.camera1 = null;
				GS.I.camera2 = null;
				counter += toDo.Length + 1;
				counter += lineLen;
			}
			else 
			{
				if (toDo.Equals("camera1 begin"))
				{
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length; 
					counter += 1;
					counter += enterLen;
					GS.I.camera1.view = ReadMatrix(toDo);

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera1.projection = ReadMatrix(toDo);

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera1.camDirection = ReadVector3(toDo);

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera1.camPosition = ReadVector3(toDo);

					counter += "camera1 end".Length + 1 + lineLen;
				}

				toDo = GetRow(toProcess, counter);

				if (toDo.Equals("camera2 begin"))
				{
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera2.view = ReadMatrix(toDo);

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera2.projection = ReadMatrix(toDo);

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera2.camDirection = ReadVector3(toDo);

					toDo = GetRow(toProcess, counter);
					counter += toDo.Length;
					counter += 1;
					counter += enterLen;
					GS.I.camera2.camPosition = ReadVector3(toDo);
					counter += "camera2 end".Length + 1 + lineLen;
				}


			}


			// Editor
			toDo = GetRow(toProcess, counter);
		
			if (toDo.Equals("editor none"))
			{
				throw new Exception(); // es ist erstmal nicht zu erwarten das man im editor mode kein editor obj gibt
			}
			else if (toDo.Equals("editor begin"))
			{
				
				counter += toDo.Length; // "editor begin".length
				counter += 1;
				counter += enterLen;
				toDo = GetRow(toProcess, counter);
			
				world = ReadMatrix(toDo);

				counter += toDo.Length + 2;
				counter += enterLen;
				counter += "editor end".Length + 1;
				counter += enterLen;


			}


			// trees
			toDo = GetRow(toProcess, counter);
			GS.I.trees = new List<Tree>();
			if (toDo.Equals("trees none"))
			{

				GS.I.trees = new List<Tree>();
			}
			else if (toDo.Equals("trees begin"))
			{
				GS.I.trees = new List<Tree>();
				counter += toDo.Length + 1; // "trees begin".length
				counter += enterLen;
				bool con = true;
				for (int i = 0; con; i++)
				{
					toDo = GetRow(toProcess, counter);
					if(toDo.Equals("tree"))
					{
						counter += toDo.Length + 1;
						counter += enterLen;
						toDo = GetRow(toProcess, counter);

						Matrix position = ReadMatrix(toDo);
						counter += toDo.Length + 1;
						counter += enterLen;

						toDo = GetRow(toProcess, counter);
						currentModel = ReatInt(toDo);

						counter += toDo.Length + 1;
						counter += enterLen;

						toDo = GetRow(toProcess, counter);
						currentList = ReatInt(toDo);

						counter += toDo.Length + 1;
						counter += enterLen;

						if (currentList == 0)
						{
							models = CM.I.trees;
						
						}
						else if (currentList == 1)
						{
							models = CM.I.rocks;
						}
						else if (currentList == 2)
						{
							models = CM.I.deadTrees;
						}
						else if (currentList == 3)
						{
							models = CM.I.bigRocks;
						}

						if(models.Length > currentModel)
						{
							GS.I.trees.Add(new Tree(models[currentModel], position, currentModel, currentList));
						}

						
							
					}
					else if(toDo.Equals("trees end"))
					{
						counter += toDo.Length + 1;
						con = false;
					}
					else
					{
						throw new Exception();
					}

				}
				currentModel = 0;

				toDo = GetRow(toProcess, counter);

			}

		

		}

		#endregion


		#region Public

		public bool GetFightMode()
		{
			return fightMode;
		}

		public void SaveScene()
		{

			string fileName = "Game";

			string text = "";

			text += WritePlayer();
			text += WriteCamera();
			text += WriteEditor();
			text += WriteTrees();

			Name = ProcessWrite(fileName, text);
			
		}

		public void LoadScene(string _fill)
		{
			string text = ProcessRead(_fill);
			Name = "load " + _fill;
			InitalizeGS(text);
		}

		public void DrawModel(Camera3rdPerson _camera)
		{

			//models[currentModel].Draw(world, _camera.view, _camera.projection);

			
			foreach (ModelMesh mesh in models[currentModel].Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = world;
					effect.View = _camera.view;
					effect.Projection = _camera.projection;
					

					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
			
		}

		public void DrawModel(CameraFightMode _camera)
		{
			foreach (ModelMesh mesh in models[currentModel].Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = world;
					effect.View = _camera.view;
					effect.Projection = _camera.projection;


					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
		}

		public void Update(GameTime _gameTime)
		{
			KeyBoardInput((float)_gameTime.ElapsedGameTime.TotalSeconds);
		}

		#endregion


		#region Events
		#endregion

	}

}