﻿using JustALittleStory.GameStates;
using JustALittleStory.Ingame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JustALittleStory
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	class Game1 : Game
	{



		#region Variables		

		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		private State currentState;
		private State nextState;

		public static Effect effect;

		#endregion

		#region Constructor

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			
			Content.RootDirectory = "Content";

			graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
			graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
			
		}

		#endregion

		#region Helpers		

		protected override void Initialize()
		{
			IsMouseVisible = true;
			graphics.GraphicsDevice.BlendState = BlendState.NonPremultiplied;
			graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
			LoadCM();

			CM.I.spriteFont = Content.Load<SpriteFont>("Font/Font01");
			GS.I.ground = new Ground(Content.Load<Model>("Models/Character/plain"));
			currentState = new MainMenuState(this, null, graphics.GraphicsDevice, Content);

			//GS.I.playerFight = new PlayerFightMode(Content.Load<Model> ("Models/Character/CubeB"));
			//GS.I.cameraFight = new CameraFightMode(graphics.GraphicsDevice); 


			base.Initialize();
		}

		protected override void LoadContent()
		{
			//effect = Content.Load<Effect>("Shader/Lightning");


			spriteBatch = new SpriteBatch(GraphicsDevice);

		}

		protected override void UnloadContent()
		{
		}

		private void LoadCM()
		{
			CM.I.player = Content.Load<Model>("Models/Character/CubeB");

			CM.I.wolf = Content.Load<Model>("Models/Character/CubeB");

			CM.I.bigRocks = new Model[5];
			CM.I.bigRocks[0] = Content.Load<Model>("Models/Rocks/BigRock5");
			CM.I.bigRocks[1] = Content.Load<Model>("Models/Rocks/BigRock1");
			CM.I.bigRocks[2] = Content.Load<Model>("Models/Rocks/BigRock2");
			CM.I.bigRocks[3] = Content.Load<Model>("Models/Rocks/BigRock3");
			CM.I.bigRocks[4] = Content.Load<Model>("Models/Rocks/BigRock4");

			CM.I.rocks = new Model[25];
			CM.I.rocks[0] = Content.Load<Model>("Models/Rocks/Rock1");
			CM.I.rocks[1] = Content.Load<Model>("Models/Rocks/Rock2");
			CM.I.rocks[2] = Content.Load<Model>("Models/Rocks/Rock3");
			CM.I.rocks[3] = Content.Load<Model>("Models/Rocks/Rock4");
			CM.I.rocks[4] = Content.Load<Model>("Models/Rocks/Rock5");
			CM.I.rocks[5] = Content.Load<Model>("Models/Rocks/Rock6");
			CM.I.rocks[6] = Content.Load<Model>("Models/Rocks/Rock7");
			CM.I.rocks[7] = Content.Load<Model>("Models/Rocks/Rock8");
			CM.I.rocks[8] = Content.Load<Model>("Models/Rocks/Rock9");
			CM.I.rocks[9] = Content.Load<Model>("Models/Rocks/Rock_10");
			CM.I.rocks[10] = Content.Load<Model>("Models/Rocks/Rock_11");
			CM.I.rocks[11] = Content.Load<Model>("Models/Rocks/Rock_12");
			CM.I.rocks[12] = Content.Load<Model>("Models/Rocks/Rock_13");
			CM.I.rocks[13] = Content.Load<Model>("Models/Rocks/Rock_14");
			CM.I.rocks[14] = Content.Load<Model>("Models/Rocks/Rock_15");
			CM.I.rocks[15] = Content.Load<Model>("Models/Rocks/Rock_16");
			CM.I.rocks[16] = Content.Load<Model>("Models/Rocks/Rock_17");
			CM.I.rocks[17] = Content.Load<Model>("Models/Rocks/Rock_18");
			CM.I.rocks[18] = Content.Load<Model>("Models/Rocks/Rock_19");
			CM.I.rocks[19] = Content.Load<Model>("Models/Rocks/Rock_20");
			CM.I.rocks[20] = Content.Load<Model>("Models/Rocks/Rock_21");
			CM.I.rocks[21] = Content.Load<Model>("Models/Rocks/Rock_22");
			CM.I.rocks[22] = Content.Load<Model>("Models/Rocks/Rock_23");
			CM.I.rocks[23] = Content.Load<Model>("Models/Rocks/Rock_24");
			CM.I.rocks[24] = Content.Load<Model>("Models/Rocks/Rock_25");

			CM.I.deadTrees = new Model[6];
			CM.I.deadTrees[0] = Content.Load<Model>("Models/Trees/DeadOak1");
			CM.I.deadTrees[1] = Content.Load<Model>("Models/Trees/DeadOak2");
			CM.I.deadTrees[2] = Content.Load<Model>("Models/Trees/DeadOak3");
			CM.I.deadTrees[3] = Content.Load<Model>("Models/Trees/DeadSpruce1");
			CM.I.deadTrees[4] = Content.Load<Model>("Models/Trees/DeadSpruce2");
			CM.I.deadTrees[5] = Content.Load<Model>("Models/Trees/DeadSpruce3");

			CM.I.trees = new Model[6];
			CM.I.trees[0] = Content.Load<Model>("Models/Trees/SpruceTree1");
			CM.I.trees[1] = Content.Load<Model>("Models/Trees/SpruceTree2");
			CM.I.trees[2] = Content.Load<Model>("Models/Trees/SpruceTree3");
			CM.I.trees[3] = Content.Load<Model>("Models/Trees/OakTree1");
			CM.I.trees[4] = Content.Load<Model>("Models/Trees/OakTree2");
			CM.I.trees[5] = Content.Load<Model>("Models/Trees/OakTree3");

		}

		protected override void Update(GameTime gameTime)
		{
			if (nextState != null)
			{
				currentState = nextState;
				nextState = null;
			}

			currentState.Update(gameTime);

			//  GS.I.playerFight.Update(gameTime);
			//  GS.I.cameraFight.Update(GS.I.playerFight.playerWorldPosition);

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default; // fixec die render order 

			currentState.Draw(gameTime, spriteBatch);

			// GS.I.playerFight.DrawModel(GS.I.cameraFight);
			// GS.I.ground.DrawModel(GS.I.cameraFight); 

			base.Draw(gameTime);
		}
		#endregion

		#region Public

		public void ChangeState(State _newState)
		{
			nextState = _newState;
		}

		#endregion


		#region Events
		#endregion






	}


}
