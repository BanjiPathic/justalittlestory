﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix WorldViewProjection;

float4x4 World;
float4x4 View;
float4x4 Projection;



struct VertexShaderInput
{
	float4 Position : POSITION0;
	//Normale

};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	//Normale
	;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 worldPosition = mul(input.Position, World);
	float4 viewPosition = mul(worldPosition, View);
	output.Position = mul(viewPosition, Projection);
	output.Color = float4(0, 1, 0, 1);
	//output.normal; g1_Pos zeug für view/word
	return output;

}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float ambientLight = 0.5f;
	float3 ambient = ambientLight * float3(1, 1, 1);
	float3 res = ambient * input.Color;

	return float4(res, 1)

}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};