﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class PlayerFightMode
	{


		#region Variables		

		Model player;
		KeyboardState lastKState;
		MouseState oldmstate;
		float speed = 100f;
		float rotationRate = 0.001f;
		public Matrix playerWorldPosition;

		int level;
		double curExp;
		double levelExp;
		bool active;
		double givenExp;
		//int[] attacks;
		//HP, MP, Atk, Def, Spd
		int[] baseStats = new int[5];
		int[] curStats = new int[5];



		#endregion


		#region Constructor

		public PlayerFightMode(Model _model)
		{

			player = _model;

			playerWorldPosition = Matrix.Identity;

			lastKState = Keyboard.GetState();

		}

		#endregion


		#region Helpers

		protected void MouseInitialize()
		{
			Mouse.SetPosition(0, 0);
			oldmstate = Mouse.GetState();
		}

		#endregion


		#region Public

		public void Update(GameTime _gameTime)
		{
			
		}

		public void SetStartValues(int[] _baseStats, int _level, double _levelExp, double _givenExp)
		{
			baseStats[0] = _baseStats[0];
			baseStats[1] = _baseStats[1];
			baseStats[2] = _baseStats[2];
			baseStats[3] = _baseStats[3];
			baseStats[4] = _baseStats[4];

			curStats[0] = baseStats[0];
			curStats[1] = baseStats[1];
			curStats[2] = baseStats[2];
			curStats[3] = baseStats[3];
			curStats[4] = baseStats[4];

			level = _level;
			curExp = 0;
			levelExp = _levelExp;

			givenExp = _givenExp;
		}

		public void SetStartValues(int[] _baseStats, int[] _curStats, int _level, double _levelExp, double _givenExp)
		{
			SetStartValues(_baseStats, _level, _levelExp, _givenExp);
			curStats[0] = _curStats[0];
			curStats[1] = _curStats[1];
			curStats[2] = _curStats[2];
			curStats[3] = _curStats[3];
			curStats[4] = _curStats[4];
		}

		public void SetStartValues()
		{
			baseStats[0] = 100;
			baseStats[1] = 100;
			baseStats[2] = 10;
			baseStats[3] = 10;
			baseStats[4] = 10;
		}


		public void Attack()
		{

		}

		public void Skill()
		{

		}

		public void Defend()
		{

		}

		public void Item()
		{

		}

		public void Flee()
		{

		}

		public void TakeDamage(int amount)
		{
			curStats[0] -= amount;

			if (curStats[0] <= 0)
			{
				curStats[0] = 0;
				KnockOut();
			}

		}

		public void KnockOut()
		{
			active = false;
			//change animation/remove from battle
		}

		public void GetExp(double amount)
		{
			curExp += amount;
			while (curExp >= levelExp)
			{
				curExp -= levelExp;
				RaiseLevel();
			}
		}

		public void RaiseLevel()
		{
			level++;
			levelExp = level * 100;
		}



		public void DrawModel(CameraFightMode camera)
		{

			foreach (ModelMesh mesh in player.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = playerWorldPosition;
					effect.View = camera.view;
					effect.Projection = camera.projection;


					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
		}

		public Matrix GetWorldposition()
		{
			return playerWorldPosition;
		}


		#endregion


		#region Events
		#endregion


	}

}
