﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Tree
	{

		#region Variables

		Model model;
		public Matrix position;
		public int modelID;
		public int listID;

		#endregion

		#region Constructor

		public Tree(Model _model, Vector3 _position, int _modelID, int _listID)
		{
			model = _model;

			modelID = _modelID;

			listID = _listID;

			position += Matrix.CreateTranslation(_position);

		}

		public Tree(Model _model, Matrix _position, int _modelID, int _listID)
		{
			model = _model;
			modelID = _modelID;
			listID = _listID;
			position = _position;

		}

		#endregion

		#region Helpers
		#endregion


		#region Public

		public void DrawModel(Camera3rdPerson _camera)
		{
			foreach (ModelMesh mesh in model.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = position;
					effect.View = _camera.view;
					effect.Projection = _camera.projection;


					effect.Alpha = 1.0f;
					mesh.Draw();
				}

				

			}
		}
		public void DrawModel(CameraFightMode _camera)
		{

			

			foreach (ModelMesh mesh in model.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = position;
					effect.View = _camera.view;
					effect.Projection = _camera.projection;


					effect.Alpha = 1.0f;
					mesh.Draw();
				}



			}
		}

		#endregion


		#region Events
		#endregion

	}

}
