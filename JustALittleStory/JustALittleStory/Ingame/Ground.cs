﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Ground
	{

		#region Variables

		Model model;
		Matrix position;



		#endregion

		#region Constructor

		public Ground(Model _model)
		{
			model = _model;
			position = Matrix.Identity;
			position += Matrix.CreateTranslation(new Vector3(0, 0, 0));
		}


		#endregion

		#region Helpers
		#endregion


		#region Public

		public void DrawModel(Camera3rdPerson _camera)
		{
			model.Draw(position, _camera.view, _camera.projection);
		}

		public void DrawModel(CameraFightMode _camera)
		{
			model.Draw(position, _camera.view, _camera.projection);
		}

		#endregion


		#region Events
		#endregion


	}

}
