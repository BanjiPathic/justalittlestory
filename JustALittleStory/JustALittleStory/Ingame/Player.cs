﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Player
	{


		#region Variables		

		Model player;
		Matrix[] playerTransforms;
		KeyboardState lastKState;
		MouseState oldmstate;
		//Vector2 mousePosition;

		float speed = 100f;
		float rotationRate = 0.001f;

		public Matrix playerWorldPosition;

		Companion wolf;

		#endregion

		#region Constructor

		public Player(Model _model)
		{
			
			player = _model;

			playerWorldPosition = Matrix.Identity;
			playerTransforms = new Matrix[_model.Bones.Count];
			_model.CopyAbsoluteBoneTransformsTo(playerTransforms);


			wolf = null;

			lastKState = Keyboard.GetState();

		}

		#endregion

		#region Helpers
		
		protected void MouseInitialize()
		{
			Mouse.SetPosition(0, 0);
			oldmstate = Mouse.GetState();
		}
		

		#endregion


		#region Public

		public void Update(GameTime _gameTime)
		{
			KeyboardState kstate = Keyboard.GetState();
			MouseState newmstate = Mouse.GetState();

			float dt = (float)_gameTime.ElapsedGameTime.TotalSeconds;

			// Player Movement WASD
			if (kstate.IsKeyDown(Keys.W))
			{
				playerWorldPosition *= Matrix.CreateTranslation(playerWorldPosition.Forward * speed * dt);
				wolf.Move(Matrix.CreateTranslation(playerWorldPosition.Forward * speed * dt), dt);
			}
			if (kstate.IsKeyDown(Keys.S))
			{
				playerWorldPosition *= Matrix.CreateTranslation(playerWorldPosition.Backward * speed * dt);
				wolf.Move(Matrix.CreateTranslation(playerWorldPosition.Backward * speed * dt), dt);
			}
			if (kstate.IsKeyDown(Keys.A))
			{
				playerWorldPosition *= Matrix.CreateTranslation(playerWorldPosition.Left * speed * dt);
				wolf.Move(Matrix.CreateTranslation(playerWorldPosition.Left * speed * dt), dt);
			}
			if (kstate.IsKeyDown(Keys.D))
			{
				playerWorldPosition *= Matrix.CreateTranslation(playerWorldPosition.Right * speed * dt);
				wolf.Move(Matrix.CreateTranslation(playerWorldPosition.Right * speed * dt), dt);
			}


			// Player Rotation


			Vector3 temp = playerWorldPosition.Translation;
			playerWorldPosition.Translation = Vector3.Zero;

			float yTemp = newmstate.Y - oldmstate.Y;
			playerWorldPosition *= Matrix.CreateRotationY(yTemp * rotationRate * dt);
			playerWorldPosition.Translation = temp;
			
			float xTemp = newmstate.X- oldmstate.X;
			playerWorldPosition *= Matrix.CreateRotationZ(xTemp * -rotationRate * dt);
			playerWorldPosition.Translation = temp;





			/*
			if (kstate.IsKeyDown(Keys.Right))
			{
				Vector3 temp = playerWorldPosition.Translation;
				playerWorldPosition.Translation = Vector3.Zero;
				playerWorldPosition *= Matrix.CreateRotationY(rotationRate * dt);
				playerWorldPosition.Translation = temp;
				wolf.Rotate(-rotationRate * dt, playerWorldPosition);

			}
			if (kstate.IsKeyDown(Keys.Left))
			{
				Vector3 temp = playerWorldPosition.Translation;
				playerWorldPosition.Translation = Vector3.Zero;
				playerWorldPosition *= Matrix.CreateRotationY(-rotationRate * dt);
				playerWorldPosition.Translation = temp;
				wolf.Rotate(rotationRate * dt, playerWorldPosition);
			}
			*/
			if (kstate.IsKeyDown(Keys.Up))
			{
				GS.I.camera1.Zoom(1);
			}
			if (kstate.IsKeyDown(Keys.Down))
			{
				GS.I.camera1.Zoom(-1);
			}


			if(newmstate.RightButton != ButtonState.Pressed)
			{
				oldmstate = newmstate;
			}
	

			lastKState = kstate;
		}

		public void DrawModel(Camera3rdPerson camera)
		{
			
			foreach (ModelMesh mesh in player.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = playerWorldPosition;
					effect.View = camera.view;
					effect.Projection = camera.projection;

					if (wolf != null) effect.DiffuseColor = (new Vector3(1, 0, 0));

					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
		}

		public Matrix GetWorldposition()
		{
			return playerWorldPosition;
		}

		public void setWolf(Companion _wolf)
		{
			wolf = _wolf;

		}



		#endregion


		#region Events
		#endregion





	}

}
