﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Companion
	{
		#region Variables
		Model companionModel;
		Matrix worldPosition;
		Matrix companionPlace;
		float offSet = 2.0f;
		float speed = 100f;
		#endregion

		#region Constructor
		public Companion(Model _compModel)
		{
			companionModel = _compModel;
		}

		public Companion(Model _compModel, Matrix _compPlace)
		{
			companionModel = _compModel;
			worldPosition = _compPlace;
			companionPlace = _compPlace;
		}
		#endregion

		#region Public
		public void Move(Matrix _translation, float _elapsedTime)
		{
			companionPlace *= _translation;

			//Follow(_elapsedTime);
			worldPosition = companionPlace;

		}



		public void Return(float _elapsedTime)
		{
			Vector3 distanceVec = companionPlace.Translation - worldPosition.Translation;
			float distance = distanceVec.Length();

			if (distance > 0.1f)
			{
				distanceVec.Normalize();
				worldPosition *= Matrix.CreateTranslation(distanceVec * speed * _elapsedTime);
			}
		}

		public void Rotate(float _rotationRate, Matrix _playerWorldPosition)
		{
			companionPlace.Translation -= _playerWorldPosition.Translation;
			companionPlace *= Matrix.CreateRotationY(_rotationRate);
			companionPlace.Translation += _playerWorldPosition.Translation;

			worldPosition = companionPlace;
			//Follow(_elapsedTime);
		}

		public void SetPosition(Matrix _playerPosition)
		{
			worldPosition = _playerPosition + Matrix.CreateTranslation(Matrix.Identity.Right * 2);
		}

		public void DrawModel(Camera3rdPerson camera)
		{

			foreach (ModelMesh mesh in companionModel.Meshes)
			{
				int i = 0;
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = worldPosition;
					effect.View = camera.view;
					effect.Projection = camera.projection;
					if (i < 3) effect.DiffuseColor = new Vector3(0, 0, 1);
					i++;
				}

				mesh.Draw();

			}
		}
		#endregion

		#region private
		private void Follow(float _elapsedTime)
		{
			Vector3 distanceVec = companionPlace.Translation - worldPosition.Translation;
			float distance = distanceVec.Length();

			if (distance >= offSet)
			{
				distanceVec.Normalize();
				worldPosition *= Matrix.CreateTranslation(distanceVec * speed * _elapsedTime);
				//worldPosition = companionPlace;
			}
		}
		#endregion
	}

}
