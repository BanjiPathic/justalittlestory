﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Enemy
	{
		
		#region Variables		

		Model model;

	

		float speed = 100f;
		float rotationRate = 0.001f;


		public Matrix playerWorldPosition;

		#endregion


		#region Constructor

		public Enemy(Model _model, Matrix _position)
		{

			model = _model;

			playerWorldPosition = _position;


		}

		#endregion


		#region Helpers


		#endregion


		#region Public

		public void Update(GameTime _gameTime)
		{

		}

		public void DrawModel(CameraFightMode camera)
		{

			foreach (ModelMesh mesh in model.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = playerWorldPosition;
					effect.View = camera.view;
					effect.Projection = camera.projection;


					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
		}

		public Matrix GetWorldposition()
		{
			return playerWorldPosition;
		}


		#endregion


		#region Events
		#endregion


	}

}
