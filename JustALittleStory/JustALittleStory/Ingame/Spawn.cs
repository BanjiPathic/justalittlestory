﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Spawn
	{
		#region Variables

		Matrix position;
		int spawnLimit;
		int spawntime;

		#endregion

		#region Constructors
		
		public Spawn(Matrix _position, int _spawnLimit, int _spawnTime)
		{
			position = _position;
			spawnLimit = _spawnLimit;
			spawntime = _spawnTime;
		}

		#endregion

		#region Public

		public void Update(GameTime _gameTime)
		{

		}



		#endregion



	}
}
