﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class CollisionTester
	{

		#region Variables

		BoundingSphere sphere1;
		BoundingSphere sphere2;

		BoundingBox box1;
		#endregion

		#region Collisionsmethoden


		public bool IsModelsCollision(Model _model1, Matrix _world1, Model _model2, Matrix _world2)
		{
			for (int meshIndex1 = 0; meshIndex1 < _model1.Meshes.Count; meshIndex1++)
			{
				sphere1 = _model1.Meshes[meshIndex1].BoundingSphere;
				sphere1 = sphere1.Transform(_world1);

				for (int meshIndex2 = 0; meshIndex2 < _model2.Meshes.Count; meshIndex2++)
				{
					sphere2 = _model2.Meshes[meshIndex2].BoundingSphere;
					sphere2 = sphere2.Transform(_world2);



					if (sphere1.Intersects(sphere2))
						return true;
				}
			}
			return false;
		}

		public bool IsModelObjectCollision(Model _model1, Matrix _world1, Model _model2, Matrix _world2)
		{
			for (int meshIndex1 = 0; meshIndex1 < _model1.Meshes.Count; meshIndex1++)
			{
				sphere1 = _model1.Meshes[meshIndex1].BoundingSphere;
				sphere1 = sphere1.Transform(_world1);


				//BoxMagic


				for (int meshIndex2 = 0; meshIndex2 < _model2.Meshes.Count; meshIndex2++)
				{
					sphere2 = _model2.Meshes[meshIndex2].BoundingSphere;
					sphere2 = sphere2.Transform(_world2);
					Vector3 center = sphere2.Center;
					float radius = sphere2.Radius;
					Vector3 offset = new Vector3(radius, radius, radius);

					box1 = new BoundingBox(center - offset, center + offset);

					if (sphere1.Intersects(box1)) return true;
				}









			}
			return false;
		}

		#endregion




	}

}
