﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Camera3rdPerson
	{
		GraphicsDevice graphicsDevice;

		public Vector3 camPosition, camDirection;
		public Matrix view, projection;

		// Position der Kamera -> Variierbar 
		// Benjis favorit off sets
		float camTrailingDistance = 20;
		float camRightLeftOffset = 0;

		float camVerticalOffset = 7;
		float camLookAhead = 10;

		public Camera3rdPerson(GraphicsDevice graphicsDevice)
		{
			this.graphicsDevice = graphicsDevice;

			float nearClipPlane = 1;
			float farClipPlane = 100000;
			float aspectRatio = graphicsDevice.Viewport.Width / (float)graphicsDevice.Viewport.Height;

			view = Matrix.CreateLookAt(camPosition, camDirection, Vector3.Up);
			projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, nearClipPlane, farClipPlane);

		}


		public void Update(Matrix _playerWorldPosition)
		{

			camPosition = _playerWorldPosition.Translation + (_playerWorldPosition.Backward * camTrailingDistance) +
														 (_playerWorldPosition.Right * camRightLeftOffset) +
														 (_playerWorldPosition.Up * camVerticalOffset);

			camDirection = _playerWorldPosition.Translation + (_playerWorldPosition.Forward * camLookAhead);

			view = Matrix.CreateLookAt(camPosition, camDirection, Vector3.Up);

		}


		public void Zoom(int _scale)
		{
			if(_scale > 0)
			{
				camVerticalOffset += 0.5f;
			}
			else if(_scale < 0)
			{
				camVerticalOffset -= 0.5f;
			}
		}


	}



}
