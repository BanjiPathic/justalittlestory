﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class CameraFightMode
	{
		#region Variables
		GraphicsDevice graphicsDevice;

		public Vector3 camPosition, camDirection;
		public Matrix view, projection;



		//  Position der Kamera -> Variierbar 

		float camTrailingDistance = 15;
		float camRightLeftOffset = 0;

		float camVerticalOffset = 35;
		float camLookAhead = 10;


		#endregion

		#region Constructor 
		public CameraFightMode(GraphicsDevice graphicsDevice)
		{

			this.graphicsDevice = graphicsDevice;

			float nearClipPlane = 1;
			float farClipPlane = 100000;
			float aspectRatio = graphicsDevice.Viewport.Width / (float)graphicsDevice.Viewport.Height;

			view = Matrix.CreateLookAt(camPosition, camDirection, Vector3.Up);
			projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, nearClipPlane, farClipPlane);

		}
		#endregion

		#region Public
		public void Update(Matrix _playerWorldPosition)
		{

			camPosition = (_playerWorldPosition.Backward * camTrailingDistance) + (_playerWorldPosition.Right * camRightLeftOffset) + (_playerWorldPosition.Up * camVerticalOffset);

			camDirection = /*_playerWorldPosition.Translation +*/(_playerWorldPosition.Forward * camLookAhead);

			view = Matrix.CreateLookAt(camPosition, camDirection, Vector3.Up);

		}

		#endregion


	}

}
