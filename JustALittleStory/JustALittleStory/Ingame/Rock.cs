﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class Rock
	{

		#region Variables

		Model model;
		public Matrix position;
		public int modelID;
		public int listID;

		#endregion

		#region Constructor

		public Rock(Model _model, Vector3 _position, int _modelID, int _listID)
		{
			model = _model;

			modelID = _modelID;

			listID = _listID;

			position += Matrix.CreateTranslation(_position);

		}

		public Rock(Model _model, Matrix _position, int _modelID, int _listID)
		{
			model = _model;
			modelID = _modelID;
			listID = _listID;
			position = _position;

		}

		#endregion

		#region Helpers
		#endregion


		#region Public

		public void DrawModel(Camera3rdPerson camera)
		{
			
			foreach (ModelMesh mesh in model.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = position;
					effect.View = camera.view;
					effect.Projection = camera.projection;


					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
		}
		public void DrawModel(CameraFightMode camera)
		{

			foreach (ModelMesh mesh in model.Meshes)
			{
				foreach (BasicEffect effect in mesh.Effects)
				{
					effect.EnableDefaultLighting();

					effect.World = position;
					effect.View = camera.view;
					effect.Projection = camera.projection;


					effect.Alpha = 1.0f;
				}

				mesh.Draw();

			}
		}
		#endregion


		#region Events
		#endregion

	}
}
