﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class GS // GameSingleton 
	{
		#region Variables

		public static GS i;

		public Player player;

		public PlayerFightMode fightPlayer;

		public Editor.Editor editor;
		public Camera3rdPerson camera1;
		public CameraFightMode camera2;
		public List<Tree> trees;
		public Ground ground;


		public List<Matrix> spawnPoints;


		public List<Enemy> enemies;

		//public List<Rock> rockes;


		public Companion wolf;

		#endregion

		#region Constructor

		private GS()
		{

		}

		#endregion

		#region Helpers
		#endregion

		#region Public

		public static GS I
		{
			get
			{
				if (i == null)
					i = new GS();
				return i;
			}
		}


		#endregion

		#region Events
		#endregion

	}

}
