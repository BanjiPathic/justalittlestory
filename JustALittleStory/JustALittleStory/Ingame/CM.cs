﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.Ingame
{
	class CM // Content Manager Classe
	{
		#region Variables

		public static CM i;

		public SpriteFont spriteFont;

		public Model player;
		public Model wolf;

		public Model[] bigRocks;
		public Model[] rocks;
		public Model[] trees;
		public Model[] deadTrees;

		#endregion

		#region Constructor

		private CM()
		{

		}

		#endregion

		#region Helpers
		#endregion


		#region Public

		public static CM I
		{
			get
			{
				if (i == null)
					i = new CM();
				return i;
			}
		}

		#endregion


		#region Events
		#endregion
	}

}
