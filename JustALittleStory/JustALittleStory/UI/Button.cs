﻿using JustALittleStory.Ingame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.UI
{
	class Button : Component
	{
		#region Variables

		private MouseState currentMouse;
		private bool isMoving = false;
		private MouseState prevMouse;
		private Texture2D texture;

		public event EventHandler Click;
		public bool clicked { get; private set; }
		public Color penColor;
		//public Vector2 position;
		public Rectangle destinationRect;

		public String text;

		#endregion

		#region Constructor

		public Button(Texture2D _texture, Vector2 _position, String _text)
		{
			texture = _texture;
			penColor = Color.Black;

			destinationRect = new Rectangle((int)_position.X, (int)_position.Y, texture.Width, texture.Height);
			text = _text;
		}

		#endregion

		#region Helpers
		#endregion


		#region Public		

		public override void Draw(GameTime _gameTime, SpriteBatch _spriteBatch)
		{
			var color = Color.White;

			if (isMoving)
			{
				color = Color.Gray;
			}
			_spriteBatch.Draw(texture, destinationRect, color);

			if (!string.IsNullOrEmpty(text))
			{
				var x = (destinationRect.X + (destinationRect.Width / 2)) - (CM.I.spriteFont.MeasureString(text).X / 2);
				var y = (destinationRect.Y + (destinationRect.Height / 2)) - (CM.I.spriteFont.MeasureString(text).Y / 2);

				_spriteBatch.DrawString(CM.I.spriteFont, text, new Vector2(x, y), penColor);

			}
		}

		public override void Update(GameTime _gameTime)
		{
			prevMouse = currentMouse;

			currentMouse = Mouse.GetState();

			var mouseRect = new Rectangle(currentMouse.X, currentMouse.Y, 1, 1);

			if (mouseRect.Intersects(destinationRect))
			{
				isMoving = true;
				if (currentMouse.LeftButton == ButtonState.Released && prevMouse.LeftButton == ButtonState.Pressed)
				{
					Click?.Invoke(this, new EventArgs());
				}
			}
			else
			{
				isMoving = false;
			}
		}

		#endregion


		#region Events
		#endregion

	}

}
