﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustALittleStory.UI
{

	public abstract class Component
	{

		#region Public

		public abstract void Draw(GameTime _gameTime, SpriteBatch _spriteBatch);
		public abstract void Update(GameTime _gameTime);

		#endregion

	}

}
